# ecoc-forumtest

## Project setup
```
composer update
```
### env setup
```
file .env.example rename .env and config database for local 
```
### database setup
```
php artisan migrate:refresh --seed
```
### Compiles and hot-reloads for development
```
php artisan serve  

##